// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).

	// LAB 4: Your code here.

	if (!(err & FEC_WR) || !(uvpt[PGNUM(addr)] & PTE_COW))
		panic("pgfault: not a write on a COW page"); 

	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

	// LAB 4: Your code here.
	if (sys_page_alloc(0, (void *) PFTEMP, PTE_W | PTE_U | PTE_P))
		panic("pgfault: sys_page_alloc failure");

	memmove((void *) PFTEMP, (void *)ROUNDDOWN((uint32_t)addr, PGSIZE), PGSIZE);

	if (sys_page_map(0, (void *)PFTEMP, 0, (void *)ROUNDDOWN((uint32_t)addr, PGSIZE), PTE_W | PTE_U | PTE_P))
		panic("pgfault: sys_page_map failure");
	
		
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{
	int r;
	//Lab 5 code
	if (uvpt[pn] & PTE_SHARE) {
		if (sys_page_map(0, (void *)(pn*PGSIZE), envid, (void*)(pn*PGSIZE), PGOFF(uvpt[pn])&PTE_SYSCALL))
			panic("duppage: sys_page_map failure");
		return 0;
	}
	//Lab 4 code
	if (!(uvpt[pn] & (PTE_W | PTE_COW))) {
		if (sys_page_map(0, (void *)(pn*PGSIZE), envid, (void *)(pn*PGSIZE), PGOFF(uvpt[pn])&PTE_SYSCALL)) 
			panic("duppage: sys_page_map failure");
	} else {
		if (sys_page_map(0, (void *)(pn*PGSIZE), envid, (void *)(pn*PGSIZE), PTE_P|PTE_U|PTE_COW))
			panic("duppage: sys_page_map failure");
		//change our perms
		if (sys_page_map(0, (void *)(pn*PGSIZE), 0, (void *)(pn*PGSIZE), PTE_P|PTE_U|PTE_COW))
			panic("duppage: sys_page_map failure");
	}
 	
	return 0;
}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
	set_pgfault_handler(pgfault);
	int envid = sys_exofork();
	if (envid > 0) {
		uint32_t i;
		for (i = 0; i < PGNUM(UTOP); i++) {
			if ((uvpd[i >> 10] & PTE_P) && i * PGSIZE == UXSTACKTOP - PGSIZE) {
				//copy stack (pgfault can't, it's working on it!)
				if (sys_page_alloc(envid, (void *)(UXSTACKTOP - PGSIZE), PTE_P|PTE_U|PTE_W))
					panic("fork: sys_page_alloc failure");

				if (sys_page_map(envid, (void *)(UXSTACKTOP - PGSIZE), 0, (void *)PFTEMP, PTE_P|PTE_U|PTE_W))
					panic("fork: sys_page_map failure");
				memmove((void *)(UXSTACKTOP - PGSIZE), (void *)PFTEMP, PGSIZE);

				sys_page_unmap(0, (void *)PFTEMP);
			}	
			else if ((uvpd[i >> 10] & PTE_P) && uvpt[i] & PTE_P) 
				duppage(envid, i);
		}
		if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
			panic("fork: sys_env_set_pgfault_upcall failure");

		if (sys_env_set_status(envid, ENV_RUNNABLE))
			panic("fork: sys_env_set_status failure");
	
	} else if (envid == 0) {
		thisenv = &envs[ENVX(sys_getenvid())];
	}

	return envid;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
