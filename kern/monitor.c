// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line


struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	{ "backtrace", "Display backtrace information", mon_backtrace },
	{ "showmappings", "Display information about page mapping", mon_showmappings },
	{ "addpermissions", "Adds perm bits to specified address range", mon_addpermissions },
	{ "setpermissions", "Sets perm bits to specified address range", mon_setpermissions },
	{ "clearpermissions", "Clears all permission bits (except PTW_P)", mon_clearpermissions },
	{ "backtrace", "Display information about the backtrace", mon_backtrace}
};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

#define FORMAT_LENGTH 80
#define EBP(_v) ((uint32_t)_v)
#define EIP(_ebp) ((uint32_t)*(_ebp+1))
#define ARG(_v,_cnt) ((uint32_t)*(_v+((_cnt)+2)))

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	int32_t cnt = 0;
	uint32_t *addr = 0;
	char format[FORMAT_LENGTH] = { 0 };
	char formatName[FORMAT_LENGTH] = { 0 };
	struct Eipdebuginfo info;
	strcpy (format, "  ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n");
	strcpy (formatName, "          %s:%d: %.*s+%d\n");
	addr = (uint32_t *) read_ebp ();
	cprintf ("Stack backtrace\n");
	for (; NULL != addr; cnt++)
	{
		cprintf (format, EBP(addr), EIP(addr), ARG(addr, 0), ARG(addr, 1),
			ARG(addr, 2), ARG(addr, 3), ARG(addr, 4));
		debuginfo_eip (EIP(addr), &info);
		cprintf (formatName,
			info.eip_file,
			info.eip_line,
			info.eip_fn_namelen,
			info.eip_fn_name, EIP(addr) - info.eip_fn_addr);
		//Trace the linked list implemented by Stack.
		addr = (uint32_t *) * addr;
	}
	return 0;
}

#include <kern/pmap.h>

int 
mon_showmappings(int argc, char **argv, struct Trapframe *tf)
{
	//range of addresses
	if (argc == 3) {
		int i;
		for (i = (int)strtol(argv[1], NULL, 0); i <= strtol(argv[2], NULL, 0); i+=PGSIZE) {
			pte_t *pt_entry = pgdir_walk(kern_pgdir, (void *)i, 0);
			if (pt_entry && (*pt_entry & PTE_P)) {
				cprintf("Page mapped from %08x is at %08x\n", i, PTE_ADDR(*pt_entry));

				int perm;
				cprintf("\tPerms: ");
				for (perm = 11; perm >= 0 ; perm--) 
					cprintf("%d", (*pt_entry & (1<<perm))/(1<<perm));
				cprintf("\n");
			}
			else
				cprintf("Page mapped from %08x is not mapped\n", i);
		}
	}
	return 0;		
}

int 
mon_addpermissions(int argc, char **argv, struct Trapframe *tf)
{	
	//if permission and virtual address range are specified
	if (argc == 4) {
		int i;
		int perms = (int)strtol(argv[3], NULL, 2);

		for (i = (int)strtol(argv[1], NULL, 0); i <= strtol(argv[2], NULL, 0); i+=PGSIZE) {
			pte_t *pt_entry = pgdir_walk(kern_pgdir, (void *)i, 0);
			if (pt_entry && (*pt_entry & PTE_P)) 
				*pt_entry |= (perms & 0xFFF);
		}
	}
	return 0;		
}

int 
mon_setpermissions(int argc, char **argv, struct Trapframe *tf)
{
	//if permission and virtual address range are specified
	if (argc == 4) {
		int i;
		int perms = (int)strtol(argv[3], NULL, 2);

		for (i = (int)strtol(argv[1], NULL, 0); i <= strtol(argv[2], NULL, 0); i+=PGSIZE) {
			pte_t *pt_entry = pgdir_walk(kern_pgdir, (void *)i, 0);
			//keep present bit!
			if (pt_entry && (*pt_entry & PTE_P)) 
				*pt_entry = PTE_ADDR(*pt_entry) | (perms & 0xFFF) | PTE_P;
		}
	}
	return 0;
}

int mon_clearpermissions(int argc, char **argv, struct Trapframe *tf)
{
	if (argc == 3) {
		argv[0] = "setpermissions";
		argv[3] = "0";
		return mon_setpermissions(argc+1, argv, tf);
	}
	return 0;
}

/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
